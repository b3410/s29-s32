// [SECTION] Dependecies and Module
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');

	const auth = require('../auth');

// [SECTION] Functionalities [Create]
	//Create
		module.exports.registerUser = (data) => {
			let fName = data.firstName;
			let lName= data.lastName;
			let email= data.email;		
			let passW= data.password;		
			let mobil= data.mobileNo;

			let newUser = new User ({
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(passW, 10),
				mobileNo: mobil
			});

			return newUser.save().then((user, err) => {
				if (user) {
					return user;
				} else {
					return false;
				}
			});
		};


	//Email Checker
		module.exports.checkEmailExists = (reqBody) => {
			return User.find({email: reqBody.email}).then(result => {
				if (result.length > 0) {
					return 'Email Alreay Exists'
				} else {
					return 'Email is still available.'
				};
			});
		};

	//Login (User Authentication)
		module.exports.loginUser = (reqBody) => {
			let email = reqBody.email;
			let pass = reqBody.password;
			return User.findOne({email: email}).then(result => {
				if (result === null) {
					return false;
				} else {
				let passW = result.password;
					const isMatched = bcrypt.compareSync(pass, passW);
		
					if (isMatched) {
						console.log(result);
						let data = result.toObject();
						console.log(data)
						return {access: auth.createAccessToken(data)};
					} else {
						return false;
					}
				};
			});
		};
		
		module.exports.enroll = async (data) => {
		      let id = data.userId;
		      let course = data.courseId;

		      let isUserUpdated = await User.findById(id).then(user => {
		         user.enrollments.push({courseId: course});
		         return user.save().then((save, error) => {
		             if (error) {
		                return false;
		             } else {
		                return true; 
		             }
		         });
		      });

		     let isCourseUpdated = await Course.findById(course).then(course => {
		           course.enrollees.push({userId: id});

		           return course.save().then((saved, err) => {
		              if (err) {
		                 return false;
		              } else {
		                 return true; 
		              }; 
		           });
		     }); 

		     if (isUserUpdated && isCourseUpdated) {
		        return true;
		     } else {
		        return 'Enrollment Failed, Go to Registrar';
		     };
		  };



// [SECTION] Functionalities [Rertieve]
	
	module.exports.getProfile = (id) => {
		return User.findById(id).then(result => {
			return result;
		});
	};

// [SECTION] Functionalities [Update] 
	//Set User as Admin
		module.exports.setAsAdmin = (userId) => {
			
			let updates = {
				isAdmin: true
			}
			return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
				if (admin) {
					return true;
				} else {
					return 'Failed to update';
				}
			});
		};

	//Set User as Non-Admin
		module.exports.setAsNonAdmin = (userId) => {

			let updates = {
				isAdmin: false
			}
			return User.findByIdAndUpdate(userId, updates).then((user, err) => {
				if (user) {
					return true;
				} else {
					return 'Failed to update'
				}
			})
		}

