//  [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

//  [SECTION] Routing Components
	const route = exp.Router()
//  [SECTION]-[POST] Route (ad)
	route.post('/', auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,
		};
		if (isAdmin) {
		controller.addCourse(data).then(outcome => {
			res.send(outcome);
		});

		} else {
			res.send('User unauthorized to proceed')
		}
	});

//  [SECTION]-[GET] Route (ad)
	route.get('/all', auth.verify, (req,res) => {
		let token = req.headers.authorization
		let payload = auth.decode(token); 
		let isAdmin = payload.isAdmin;
		(isAdmin) ?
		controller.getAllCourse().then(result => res.send(result))
		:
			res.send('Unauthorized User')
	});

	//(both)
	route.get('/', (req,res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	//(both)
	route.get('/:id', (req,res) => {
		// console.log(req.params.id);
		let data = req.params.id
		controller.getCourse(data).then(result => {
			res.send(result);
		});
	});

//  [SECTION]-[PUT] Route
	//Update Course Details (ad)
	route.put('/:courseId', auth.verify,(req, res) => {

		let params = req.params;
		let body = req.body;
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send('user unauthorized')
		} else {
			controller.updateCourse(params, body).then(outcome => {
				res.send(outcome);
			});
		};

	});

	//Course Archive (Ad)
	route.put('/:courseId/archive', auth.verify, (req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
				controller.archiveCourse(params).then(outcome => {
					res.send(outcome);
			})
			:
				res.send('Unauthorized User');

	});

//  [SECTION]-[DELETE] Route (Ad)
	route.delete('/:courseId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.courseId;
		isAdmin ?
		controller.deleteCourse(id).then(outcome => {
			res.send(outcome);
		})
		: res.send('Unauthorized User')
	});



//  [SECTION] Export Route System
	module.exports = route;